from django.contrib import admin

from hodor.models import Tournament, TournamentSubmission, UserSimulation


class TournamentSubmissionAdmin(admin.ModelAdmin):
    list_display = ('id', 'player_name', 'tournament', 'valid', 'wins', 'army_power', 'recruit_tu', 'main_stack',)
    list_filter = ('tournament',)
    exclude = ['valid', 'validation_message', 'army_power', 'recruit_tu', 'main_stack', 'wins', 'average_result_diff', 'area_taken', ]


admin.site.register(Tournament)
admin.site.register(TournamentSubmission, TournamentSubmissionAdmin)
admin.site.register(UserSimulation)
