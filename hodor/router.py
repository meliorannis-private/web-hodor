class MeliorannisRouter:
    """
    A router that redirects all "MA related" queries to MA database.
    """
    model_names = {'budovy', 'jednotky', 'kouzla'}

    DATABASE_NAME = 'meliorannis'

    def db_for_read(self, model, **hints):
        if model._meta.app_label == 'hodor' and model._meta.model_name in self.model_names:
            return self.DATABASE_NAME
        return None

    def db_for_write(self, model, **hints):
        if model._meta.app_label == 'hodor' and model._meta.model_name in self.model_names:
            return self.DATABASE_NAME
        return None
