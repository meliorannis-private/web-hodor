from typing import Iterable

from django.db import models
from django.urls import reverse
from django.utils.text import slugify


class Tournament(models.Model):
    UNIT_TYPE_ONLY_CORE = 'only_core'
    UNIT_TYPE_SINGLE_COLOR_NO_NEUTRAL = 'single_color_no_neutral'
    UNIT_TYPE_CHOICES = (
        (UNIT_TYPE_ONLY_CORE, 'ONLY_CORE'),
        (UNIT_TYPE_SINGLE_COLOR_NO_NEUTRAL, 'SINGLE_COLOR_NO_NEUTRAL'),
    )
    RESULT_TYPE_NUMBER_OF_WINS = 'number_of_wins'
    RESULT_TYPE_AREA_TAKEN = 'area_taken'
    RESULT_TYPE_CHOICES = (
        (RESULT_TYPE_NUMBER_OF_WINS, 'NUMBER_OF_WINS'),
        (RESULT_TYPE_AREA_TAKEN, 'AREA_TAKEN'),
    )

    name = models.CharField(
        max_length=128,
    )

    slug = models.CharField(
        max_length=128,
        blank=True,
    )

    tournament_date = models.DateField()

    result_type = models.CharField(
        max_length=128,
        blank=False,
        null=False,
        choices=RESULT_TYPE_CHOICES,
        default=RESULT_TYPE_NUMBER_OF_WINS,
    )

    support_xp = models.FloatField(
        null=False,
        blank=False,
        default=0.0,
    )

    use_size_bonus = models.BooleanField(default=False)

    validate_units_selection = models.CharField(
        max_length=128,
        blank=True,
        null=True,
        choices=UNIT_TYPE_CHOICES,
    )

    validate_min_xp_ranged = models.FloatField(
        null=True,
        blank=True,
    )
    validate_max_xp_ranged = models.FloatField(
        null=True,
        blank=True,
    )

    validate_min_xp_melee = models.FloatField(
        null=True,
        blank=True,
    )
    validate_max_xp_melee = models.FloatField(
        null=True,
        blank=True,
    )
    validate_max_units = models.PositiveIntegerField(
        null=True,
        blank=True,
    )
    validate_max_power = models.PositiveIntegerField(
        null=True,
        blank=True,
    )
    validate_max_turns = models.PositiveIntegerField(
        null=True,
        blank=True,
    )
    # TODO implement
    validate_max_submissions = models.PositiveIntegerField(
        default=1,
    )

    intro_markdown = models.TextField(
        blank=True,
    )
    publicly_visible = models.BooleanField(default=False)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if not self.slug:
            self.slug = slugify(self.name)

        super().save(force_insert, force_update, using, update_fields)

    def get_absolute_url(self):
        return reverse('hodor:tournament_detail', kwargs={'tournament_id': self.id, 'tournament_slug': self.slug})

    def __str__(self):
        return 'Tournament %s #%d' % (self.name, self.id)

    def result_type_submission_order_by(self) -> Iterable[str]:
        if self.result_type == self.RESULT_TYPE_NUMBER_OF_WINS:
            return ['-wins', '-average_result_diff']
        elif self.result_type == self.RESULT_TYPE_AREA_TAKEN:
            return ['-area_taken', '-average_result_diff']
        else:
            raise Exception('Illegal tournament result type %s' % self.result_type)


class TournamentSubmission(models.Model):
    tournament = models.ForeignKey(
        Tournament,
        on_delete=models.CASCADE,
    )

    player_name = models.CharField(
        max_length=128,
    )

    player_id = models.PositiveIntegerField()

    army = models.TextField()

    valid = models.BooleanField(
        null=True,
        blank=True,
    )

    validation_message = models.CharField(
        max_length=128,
        null=True,
        blank=True,
    )

    army_power = models.PositiveIntegerField(
        null=True,
        blank=True,
    )

    recruit_tu = models.PositiveIntegerField(
        null=True,
        blank=True,
    )

    main_stack = models.CharField(
        max_length=128,
        null=True,
        blank=True,
    )

    wins = models.PositiveIntegerField(default=0)
    average_result_diff = models.FloatField(default=0)
    area_taken = models.FloatField(default=0.0)

    def __str__(self):
        return 'Submission by %s#%d (%s, AT %.1f)' % (self.player_name, self.player_id, self.main_stack, self.area_taken)


class TournamentResult(models.Model):
    tournament = models.ForeignKey(
        Tournament,
        on_delete=models.CASCADE,
    )

    attacker = models.ForeignKey(
        TournamentSubmission,
        on_delete=models.CASCADE,
        related_name='+',
    )

    defender = models.ForeignKey(
        TournamentSubmission,
        on_delete=models.CASCADE,
        related_name='+',
    )

    attacker_losses = models.FloatField()
    defender_losses = models.FloatField()

    log_events_json = models.TextField()

    class Meta:
        unique_together = ('tournament', 'attacker', 'defender',)

    def get_absolute_url(self):
        return reverse('hodor:tournament_result_match', kwargs={
            'tournament_id': self.tournament.id,
            'tournament_slug': self.tournament.slug,
            'attacker_id': self.attacker_id,
            'defender_id': self.defender_id,
        })

    def __str__(self):
        return 'Tournament Result %d vs %d: %.2f : %.2f' % (
            self.attacker.id, self.defender.id,
            self.attacker_losses * 100.0, self.defender_losses * 100.0
        )


class UserSimulation(models.Model):
    created_at = models.DateTimeField(
        auto_now_add=True,
    )

    army_attacker = models.TextField()
    army_defender = models.TextField()


class Jednotky(models.Model):
    id = models.IntegerField(primary_key=True)
    jmeno = models.CharField(max_length=128)
    barva = models.CharField(max_length=1)
    specializace = models.CharField(max_length=1)
    dmg = models.FloatField()
    brneni = models.FloatField()
    zivoty = models.FloatField()
    plat_z = models.FloatField()
    plat_m = models.FloatField()
    plat_l = models.FloatField()
    rekrut_z = models.FloatField()
    rekrut_m = models.FloatField()
    rekrut_l = models.FloatField()
    iniciativa = models.SmallIntegerField()
    typ_ut = models.CharField(max_length=1)
    pohybl = models.SmallIntegerField()
    power = models.FloatField()
    druh = models.CharField(max_length=1)
    rekrut_bud = models.IntegerField()
    jedn_za_tu = models.FloatField()
    iq = models.SmallIntegerField()
    rod = models.SmallIntegerField(null=True)

    class Meta:
        managed = False
        db_table = 'jednotky'


class Budovy(models.Model):
    id = models.IntegerField(primary_key=True)
    nazev = models.CharField(max_length=64)
    max = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'budovy'


class Kouzla(models.Model):
    id = models.IntegerField(primary_key=True)
    nazev = models.CharField(max_length=150)
    barva = models.CharField(max_length=1)
    special = models.CharField(max_length=1)
    druh = models.CharField(max_length=3)
    mana_sesl = models.IntegerField()
    sesl_tu = models.SmallIntegerField()
    sesl_tu_posl = models.SmallIntegerField()
    text_pri_seslani = models.CharField(max_length=255)
    brana = models.SmallIntegerField()
    skm = models.SmallIntegerField(db_column='SKM')
    skb = models.SmallIntegerField(db_column='SKB')

    class Meta:
        managed = False
        db_table = 'kouzla'
