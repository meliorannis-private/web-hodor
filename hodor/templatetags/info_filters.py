from django import template

from hodor.data.provider import UnitFullInfo

register = template.Library()


@register.filter
def dmg_pwr(unit: UnitFullInfo) -> float:
    coeff = 1.7
    if unit.attack_type == UnitFullInfo.ATTACK_TYPE_RANGED or unit.movement == 3:
        coeff = 3.1

    elif unit.movement == 2:
        coeff = 2.5

    return unit.damage * coeff / unit.power


@register.filter
def life_pwr(unit: UnitFullInfo) -> float:
    return unit.life / unit.power


@register.filter
def armor_pwr(unit: UnitFullInfo) -> float:
    return unit.armor / unit.power


@register.filter
def upkeep_gold_1k_pwr(unit: UnitFullInfo) -> float:
    return (unit.upkeep_gold + 0.00545 * 3) / unit.power * 1000


@register.filter
def upkeep_mana_1k_pwr(unit: UnitFullInfo) -> float:
    return unit.upkeep_mana / unit.power * 1000


@register.filter
def upkeep_pop_1k_pwr(unit: UnitFullInfo) -> float:
    return unit.upkeep_pop / unit.power * 1000


@register.filter
def abs_ini_class(ini: int) -> str:
    if ini < 3:
        return 'c702020'
    if ini < 7:
        return 'c602020'
    if ini < 11:
        return 'c502020'
    if ini < 15:
        return 'c402020'
    if ini < 18:
        return 'c302020'
    if ini < 22:
        return 'c202020'
    if ini < 26:
        return 'c203020'
    if ini < 30:
        return 'c204020'
    if ini < 34:
        return 'c205020'
    if ini < 37:
        return 'c206020'
    return 'c207020'
