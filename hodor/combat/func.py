import math

AREA_20K_MUL = 20_000 ** 0.75
"""
Multiplier for taking area when an enemy has 20k.
"""


def battle_area_gain(losses_attacker: float, losses_defender: float, area_defender: int = 1) -> float:
    """
    >>> battle_area_gain(0.20, 0.22)
    0.0

    >>> battle_area_gain(0.22, 0.10)
    0.0

    >>> battle_area_gain(0.10, 0.20)  # doctest: +ELLIPSIS
    0.144764827...

    >>> battle_area_gain(0.10, 0.20, 20_000)  # doctest: +ELLIPSIS
    243.46...

    >>> battle_area_gain(0.1726, 0.3964, 11_577)  # doctest: +ELLIPSIS
    267.8...
    """

    diff = losses_defender - losses_attacker
    if diff < 0.02:
        # not a win
        return 0.0

    return area_defender ** 0.75 * diff * 10 / 3 / math.log(diff * 100)
