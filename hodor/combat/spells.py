from typing import Optional, Dict, Generator

from hodor.combat.combatmodel import SpellInCombat, Unit, UnitTargeting
from hodor.combat.logevent import CastSpell


# noinspection DuplicatedCode
class SpellHelper:

    def __init__(self, battle):
        from .battle import Battle
        self.battle = battle  # type: Battle

    def cast_spells_for_round(self):
        attacker_spell = self.battle.attacker_spells[self.battle.round]  # type: Optional[SpellInCombat]
        defender_spell = self.battle.defender_spells[self.battle.round]  # type: Optional[SpellInCombat]

        self.try_cast_check_counterspell(attacker_spell, defender_spell, True)
        self.try_cast_check_counterspell(defender_spell, attacker_spell, False)

    def try_cast_check_counterspell(self, caster_spell: Optional[SpellInCombat], opponent_spell: Optional[SpellInCombat], is_attacker: bool):
        if caster_spell is not None:
            if caster_spell.counterspell:
                # casting counterspell
                if opponent_spell is None:
                    self._log_spell_target_not_found(is_attacker=is_attacker, spell=caster_spell)
                else:
                    self.battle.log_event(CastSpell(is_attacker,
                                                    True,
                                                    spell_name=caster_spell.name,
                                                    spell_text=caster_spell.text_template.replace('xxKOUZLOxx', opponent_spell.name)))
            else:
                # casting regular spell
                if opponent_spell is not None and opponent_spell.counterspell:
                    # but was countered by enemy
                    self.battle.log_event(CastSpell(is_attacker=is_attacker, success=False, spell_name=caster_spell.name))
                else:
                    self.cast(caster_spell, caster_is_attacker=is_attacker)

    def cast(self, spell: SpellInCombat, caster_is_attacker: bool) -> None:
        spell_xp = self._spell_xp(spell, caster_is_attacker)

        if spell.spell_id == 1001:
            self.cast_1001_pozehnani(spell, spell_xp, caster_is_attacker)

        elif spell.spell_id == 1002:
            self.cast_1002_sveceni_zbrani(spell, spell_xp, caster_is_attacker)

        elif spell.spell_id == 1004:
            self.cast_1004_uzdraveni(spell, spell_xp, caster_is_attacker)

        elif spell.spell_id == 1501:
            self.cast_1501_vudcuv_pohled(spell, spell_xp, caster_is_attacker)

        elif spell.spell_id == 2001:
            self.cast_2001_kletba(spell, spell_xp, caster_is_attacker)

        elif spell.spell_id == 2002:
            self.cast_2002_smich(spell, spell_xp, caster_is_attacker)

        elif spell.spell_id == 2004:
            self.cast_2004_hrisne_sny(spell, spell_xp, caster_is_attacker)

        elif spell.spell_id == 2007:
            self.cast_2007_zran(spell, spell_xp, caster_is_attacker)

        elif spell.spell_id == 2507:
            self.cast_2507_astralni_prelud(spell, spell_xp, caster_is_attacker)

        elif spell.spell_id == 3001:
            self.cast_3001_pavucina(spell, spell_xp, caster_is_attacker)

        elif spell.spell_id == 3003:
            self.cast_3003_duha(spell, spell_xp, caster_is_attacker)

        elif spell.spell_id == 3010:
            self.cast_3010_laska_lesa(spell, spell_xp, caster_is_attacker)

        elif spell.spell_id == 3501:
            self.cast_3501_koreny(spell, spell_xp, caster_is_attacker)

        elif spell.spell_id == 4001:
            self.cast_4001_casovy_posun(spell, spell_xp, caster_is_attacker)

        elif spell.spell_id == 4002:
            self.cast_4002_spanek(spell, spell_xp, caster_is_attacker)

        elif spell.spell_id == 4005:
            self.cast_4005_jasnozrivost(spell, spell_xp, caster_is_attacker)

        elif spell.spell_id == 4501:
            self.cast_4501_fatamorgana(spell, spell_xp, caster_is_attacker)

        elif spell.spell_id == 5002:
            self.cast_5002_kamenna_kuze(spell, spell_xp, caster_is_attacker)

        elif spell.spell_id == 5006:
            self.cast_5006_okouzleni(spell, spell_xp, caster_is_attacker)

        elif spell.spell_id == 5010:
            self.cast_5010_ledovy_dest(spell, spell_xp, caster_is_attacker)

        elif spell.spell_id == 5501:
            self.cast_5501_snezna_slepota(spell, spell_xp, caster_is_attacker)

        elif spell.spell_id == 6700:
            self.cast_6700_klamne_manevry(spell, spell_xp, caster_is_attacker)

        elif spell.spell_id == 7001:
            self.cast_7001_magicka_aura(spell, spell_xp, caster_is_attacker)

        elif spell.spell_id == 7002:
            self.cast_7002_letargie(spell, spell_xp, caster_is_attacker)

        elif spell.spell_id == 7502:
            self.cast_7502_zelva(spell, spell_xp, caster_is_attacker)

        else:
            raise Exception('Unknown spell id %d' % spell.spell_id)

    def cast_1001_pozehnani(self, spell: SpellInCombat, spell_xp: float, caster_is_attacker: bool):
        effect = int(spell_xp * 10.0)
        for unit in self._alive_units(caster_is_attacker):
            unit.iniciativa += effect
            unit.iniciativa_z += effect

        self._log_spell_success(caster_is_attacker, spell, {'xxINICIATIVAxx': '%d' % effect})

    def cast_1002_sveceni_zbrani(self, spell: SpellInCombat, spell_xp: float, caster_is_attacker: bool):
        effect = 1 + spell_xp * 0.15
        for unit in self._alive_units(caster_is_attacker):
            unit.dmg_z *= effect
            unit.dmg *= effect

        self._log_spell_success(caster_is_attacker, spell, {'xxPROCENTAxx': '%d' % ((effect - 1) * 100)})

    def cast_1004_uzdraveni(self, spell: SpellInCombat, spell_xp: float, caster_is_attacker: bool):
        target = self._select_spell_target(not caster_is_attacker, spell.targeting, prefer_shooting=True)

        if target is None:
            self._log_spell_target_not_found(caster_is_attacker, spell)
            return

        effect = spell_xp * 0.06

        target.dmg_z = target.dmg_z * (1 - effect)

        self._log_spell_success(caster_is_attacker, spell, {'xxPROCENTAxx': '%d' % (effect * 100)}, target=target)

    def cast_1501_vudcuv_pohled(self, spell: SpellInCombat, spell_xp: float, caster_is_attacker: bool):
        effect = int(spell_xp * 10)

        for unit in self._alive_units(caster_is_attacker):
            unit.iniciativa += effect
            unit.iniciativa_z += effect

        self._log_spell_success(caster_is_attacker, spell, {'xxINICIATIVAxx': '%d' % effect})

    def cast_2001_kletba(self, spell: SpellInCombat, spell_xp: float, caster_is_attacker: bool):
        target = self._select_spell_target(not caster_is_attacker, spell.targeting, prefer_shooting=True)

        if target is None:
            self._log_spell_target_not_found(caster_is_attacker, spell)
            return

        effect = spell_xp * 0.4

        target.dmg_z = target.dmg_z * (1 - effect)

        self._log_spell_success(caster_is_attacker, spell, {'xxPROCENTAxx': '%d' % (effect * 100)}, target=target)

    def cast_2002_smich(self, spell: SpellInCombat, spell_xp: float, caster_is_attacker: bool):
        target = self._select_spell_target(not caster_is_attacker, spell.targeting)

        if target is None:
            self._log_spell_target_not_found(caster_is_attacker, spell)
            return

        effect = int(spell_xp * 10.0)

        target.iniciativa = max(1.0, target.iniciativa - effect)
        target.iniciativa_z = max(1.0, target.iniciativa_z - effect)

        self._log_spell_success(caster_is_attacker, spell, {'xxINICIATIVAxx': '%d' % effect}, target=target)

    def cast_2004_hrisne_sny(self, spell: SpellInCombat, spell_xp: float, caster_is_attacker: bool):
        effect = spell_xp * 0.25
        for unit in self._alive_units(not caster_is_attacker):
            unit.brneni_celkem *= (1 - effect)

        self._log_spell_success(caster_is_attacker, spell, {'xxPROCENTAxx': '%d' % (effect * 100)})

    def cast_2007_zran(self, spell: SpellInCombat, spell_xp: float, caster_is_attacker: bool):
        target = self._select_spell_target(not caster_is_attacker, spell.targeting)

        if target is None:
            self._log_spell_target_not_found(caster_is_attacker, spell)
            return

        effect = spell_xp * 0.27

        target.celkem_zivotu *= 1 - effect

        self._log_spell_success(caster_is_attacker, spell, {'xxPROCENTAxx': '%d' % (effect * 100)}, target=target)

    def cast_2507_astralni_prelud(self, spell: SpellInCombat, spell_xp: float, caster_is_attacker: bool):
        modified_xp = 100 * spell_xp + 15

        if modified_xp < 50:
            max_power = modified_xp * (modified_xp ** 0.5) * 90
        else:
            max_power = modified_xp ** 4 / 200

        target = self._select_spell_target(not caster_is_attacker, spell.targeting, extra_condition=lambda unit: unit.pohybl > 1 and unit.power * unit.pocet < max_power)

        if target is None:
            self._log_spell_target_not_found(caster_is_attacker, spell)
            return

        target.pohybl = 1

        self._log_spell_success(caster_is_attacker, spell, {'xxPROCENTAxx': int(100 * target.power * target.pocet / max_power)}, target=target)

    def cast_3001_pavucina(self, spell: SpellInCombat, spell_xp: float, caster_is_attacker: bool):
        target = self._select_spell_target(not caster_is_attacker, spell.targeting)

        if target is None:
            self._log_spell_target_not_found(caster_is_attacker, spell)
            return

        effect = int(spell_xp * 7.0)

        target.iniciativa = max(1.0, target.iniciativa - effect)
        target.iniciativa_z = max(1.0, target.iniciativa_z - effect)

        self._log_spell_success(caster_is_attacker, spell, {'xxINICIATIVAxx': '%d' % effect}, target=target)

    def cast_3003_duha(self, spell: SpellInCombat, spell_xp: float, caster_is_attacker: bool):
        target = self._select_spell_target(not caster_is_attacker, spell.targeting, extra_condition=lambda unit: unit.brneni_celkem > 0)

        if target is None:
            self._log_spell_target_not_found(caster_is_attacker, spell)
            return

        effect = spell_xp * 0.4
        target.brneni_celkem *= (1 - effect)

        self._log_spell_success(caster_is_attacker, spell, {'xxPROCENTAxx': '%d' % (effect * 100)}, target=target)

    def cast_3010_laska_lesa(self, spell: SpellInCombat, spell_xp: float, caster_is_attacker: bool):
        effect_caster = spell_xp * 0.8
        effect_enemy = spell_xp * 0.6

        resurrect_power_caster = 0.0
        resurrect_power_enemy = 0.0

        for unit in (unit for unit in self.battle.units if (unit.id_jedn < 1000 or unit.id_jedn > 2000) and unit.id_jedn not in (6026, 6012, 6019, 6022, 602)):
            if unit.is_attacker_player == caster_is_attacker:
                effect = effect_caster
            else:
                effect = effect_enemy

            dead = unit.pocet_puvodni - unit.pocet
            resurrect = int(effect * dead)
            unit.celkem_zivotu += resurrect * unit.zivoty
            unit.brneni_celkem += resurrect * unit.brneni_celkem / (unit.pocet + 1)
            unit.pocet += resurrect
            if unit.is_attacker_player == caster_is_attacker:
                resurrect_power_caster += resurrect * unit.power
            else:
                resurrect_power_enemy += resurrect * unit.power

        self._log_spell_success(caster_is_attacker, spell, {'xxPWR1xx': '%d' % resurrect_power_caster, 'xxPWR2xx': '%d' % resurrect_power_enemy})

    def cast_3501_koreny(self, spell: SpellInCombat, spell_xp: float, caster_is_attacker: bool):
        effect = int(spell_xp * 14)
        for unit in self._alive_units(not caster_is_attacker):
            unit.iniciativa = max(1.0, unit.iniciativa - effect)
            unit.iniciativa_z = max(1.0, unit.iniciativa_z - effect)

        self._log_spell_success(caster_is_attacker, spell, {'xxINICIATIVAxx': '%d' % effect})

    def cast_4001_casovy_posun(self, spell: SpellInCombat, spell_xp: float, caster_is_attacker: bool):
        effect = int(spell_xp * 12.0)
        for unit in self._alive_units(caster_is_attacker):
            unit.iniciativa += effect
            unit.iniciativa_z += effect

        self._log_spell_success(caster_is_attacker, spell, {'xxINICIATIVAxx': '%d' % effect})

    def cast_4002_spanek(self, spell: SpellInCombat, spell_xp: float, caster_is_attacker: bool):
        sum_power = self._sum_power(not caster_is_attacker)

        limit_power = sum_power * (spell_xp * 0.6) ** 2
        affected_power = 0
        affected_units = 0
        strongest_unit = None
        strongest_unit_power = 0

        for unit in self._alive_units(not caster_is_attacker, lambda unit: unit.power * unit.pocet < limit_power):
            unit.iniciativa_z = (50 - unit.iniciativa_z) / 10
            affected_power += unit.pocet * unit.power
            affected_units += 1
            if unit.pocet * unit.power > strongest_unit_power:
                strongest_unit = unit
                strongest_unit_power = unit.pocet * unit.power

        if affected_units == 0:
            self._log_spell_target_not_found(caster_is_attacker, spell)
        else:
            self._log_spell_success(caster_is_attacker, spell, {'xxPWRxx': '%d' % affected_power,
                                                                'xxPOCETxx': '%d' % affected_units,
                                                                'xxJEDNOTKAxx': strongest_unit.nazev})

    def cast_4005_jasnozrivost(self, spell: SpellInCombat, spell_xp: float, caster_is_attacker: bool):
        effect = spell_xp * 0.45
        target_found = False

        for unit in self._alive_units(not caster_is_attacker, lambda unit: unit.is_support):
            unit.nebezpecnost += unit.nebezpecnost * effect
            target_found = True

        if target_found:
            self._log_spell_success(caster_is_attacker, spell, {'xxSTITxx': '%d' % (effect * 100)})
        else:
            self._log_spell_target_not_found(caster_is_attacker, spell)

    def cast_4501_fatamorgana(self, spell: SpellInCombat, spell_xp: float, caster_is_attacker: bool):
        target = self._select_spell_target(not caster_is_attacker, spell.targeting, prefer_shooting=True)

        if target is None:
            self._log_spell_target_not_found(caster_is_attacker, spell)
            return

        effect = spell_xp * 0.4

        target.dmg_z = target.dmg_z * (1 - effect)

        self._log_spell_success(caster_is_attacker, spell, {'xxPROCENTAxx': '%d' % (effect * 100)}, target=target)

    def cast_5002_kamenna_kuze(self, spell: SpellInCombat, spell_xp: float, caster_is_attacker: bool):
        effect = 1 + spell_xp * 0.25
        for unit in self._alive_units(caster_is_attacker):
            unit.zivoty *= effect
            unit.celkem_zivotu *= effect

        self._log_spell_success(caster_is_attacker, spell, {'xxPROCENTAxx': '%d' % ((effect - 1) * 100)})

    def cast_5006_okouzleni(self, spell: SpellInCombat, spell_xp: float, caster_is_attacker: bool):
        effect = spell_xp * 0.6

        for unit in self._alive_units(caster_is_attacker, lambda unit: unit.is_shooter()):
            unit.dmg_z += unit.dmg_z * effect

        self._log_spell_success(caster_is_attacker, spell, {'xxPROCENTAxx': '%d' % (effect * 100)})

    def cast_5010_ledovy_dest(self, spell: SpellInCombat, spell_xp: float, caster_is_attacker: bool):
        effect = spell_xp * 10.0
        limit_power = effect ** 3 / 2
        affected_power = 0
        affected_units = 0
        strongest_unit = None
        strongest_unit_power = 0

        for unit in self._alive_units(not caster_is_attacker):
            unit.iniciativa_z = max(1.0, unit.iniciativa_z - effect)

            if unit.power < limit_power:
                unit.pohybl = 1

                affected_units += 1
                affected_power += unit.power * unit.pocet

                if unit.power > strongest_unit_power:
                    strongest_unit_power = unit.power
                    strongest_unit = unit

        if strongest_unit is None:
            self._log_spell_target_not_found(caster_is_attacker, spell)
        else:
            self._log_spell_success(caster_is_attacker, spell, {'xxPWRxx': int(affected_power),
                                                                'xxPOCxx': affected_units,
                                                                'xxJEDNOTKAxx': strongest_unit.nazev,
                                                                'xxINICxx': int(effect)})

    def cast_5501_snezna_slepota(self, spell: SpellInCombat, spell_xp: float, caster_is_attacker: bool):
        effect = spell_xp * 14.0

        for unit in self._alive_units(not caster_is_attacker):
            unit.iniciativa = max(1.0, unit.iniciativa - effect)
            unit.iniciativa_z = max(1.0, unit.iniciativa_z - effect)

        self._log_spell_success(caster_is_attacker, spell, {'xxINICIATIVAxx': int(effect)})

    def cast_6700_klamne_manevry(self, spell: SpellInCombat, spell_xp: float, caster_is_attacker: bool):
        stack_count = int(spell_xp / 0.16 + 0.5)

        if stack_count == 0:
            return self._log_spell_target_not_found(caster_is_attacker, spell)

        target = self._select_spell_target(caster_is_attacker, UnitTargeting.COUNT)

        if target is None:
            return self._log_spell_target_not_found(caster_is_attacker, spell)

        for i in range(stack_count):
            unit = Unit(target.is_attacker_player, target.id_jedn, '%s (návnada)' % target.nazev, target.pocet + 1,
                        1, 0, 0, 1, 0,
                        target.typ_ut, target.pohybl, target.power, target.power_at_full_xp, target.druh, target.pocet + 1, target.podle, 1.0)
            unit.temporary_for_round = True
            self.battle.units.append(unit)

        self._log_spell_success(caster_is_attacker, spell, {
            'xxSTACKUxx': stack_count,
            'xxPOCETxx': target.pocet + 1,
        })

    def cast_7001_magicka_aura(self, spell: SpellInCombat, spell_xp: float, caster_is_attacker: bool):
        effect = spell_xp * 0.2
        if caster_is_attacker:
            self.battle.attacker_spell_xp_boost += effect
        else:
            self.battle.defender_spell_xp_boost += effect

        self._log_spell_success(caster_is_attacker, spell, {'xxPROCENTAxx': '%.1f' % (effect * 100), })

    def cast_7002_letargie(self, spell: SpellInCombat, spell_xp: float, caster_is_attacker: bool):
        range_dmg_caster = 0.3
        range_ini_caster = 0.25
        range_dmg_enemy = 0.6
        range_ini_enemy = 0.5

        dmg_change_caster = range_dmg_caster * spell_xp
        ini_change_caster = range_ini_caster * spell_xp
        dmg_change_enemy = range_dmg_enemy * spell_xp
        ini_change_enemy = range_ini_enemy * spell_xp

        for unit in self._alive_units(caster_is_attacker):
            unit.dmg_z = unit.dmg_z * (1 - dmg_change_caster)
            unit.iniciativa_z = unit.iniciativa_z * (1 - ini_change_caster) - 1.5

        for unit in self._alive_units(not caster_is_attacker):
            unit.dmg_z = unit.dmg_z * (1 - dmg_change_enemy)
            unit.iniciativa_z = unit.iniciativa_z * (1 - ini_change_enemy) - 3.0

        self._log_spell_success(caster_is_attacker, spell, {'xxPROCSESxx': int(ini_change_caster * 100),
                                                            'xxDMGSESxx': int(dmg_change_caster * 100),
                                                            'xxPROCENTAxx': int(ini_change_enemy * 100),
                                                            'xxDMGxx': int(dmg_change_enemy * 100),
                                                            })

    def cast_7502_zelva(self, spell: SpellInCombat, spell_xp: float, caster_is_attacker: bool):
        target = self._select_spell_target(caster_is_attacker, spell.targeting, extra_condition=lambda unit: unit.brneni_celkem * 6 > unit.celkem_zivotu)

        if target is None:
            self._log_spell_target_not_found(caster_is_attacker, spell)
            return

        effect_armor = spell_xp * 0.30
        effect_life = spell_xp * 0.05

        target.brneni_celkem *= 1 + effect_armor
        target.brneni_celkem_puvodni *= 1 + effect_armor

        target.zivoty *= 1 + effect_life
        target.celkem_zivotu *= 1 + effect_life

        self._log_spell_success(caster_is_attacker, spell, {
            'xxPROCENTA1xx': '%.1f' % (effect_life * 100.0),
            'xxPROCENTA2xx': '%.1f' % (effect_armor * 100.0),
        }, target=target)

    def _spell_xp(self, spell: SpellInCombat, caster_is_attacker: bool):
        xp = spell.xp
        if spell.spell_id not in (5505, 7001):
            if caster_is_attacker:
                xp += self.battle.attacker_spell_xp_boost
            else:
                xp += self.battle.defender_spell_xp_boost

        if xp > 1.0:
            return 1.0

        if xp < 0.01:
            return 0.01

        return xp

    def _select_spell_target(self, target_attacker: bool, targeting: str, prefer_shooting: bool = False, extra_condition=None) -> Optional[Unit]:
        targets = list(
            unit
            for unit
            in self.battle.units
            if unit.is_attacker_player == target_attacker and unit.pocet > 0
        )

        if extra_condition is not None:
            targets = list(unit for unit in targets if extra_condition(unit))

        targets = sorted(
            targets,
            key=lambda unit: unit.target_selection_rank(targeting, use_nebezpecnost=False) * self._target_selection_rank_shooter_bonus(unit, prefer_shooting),
            reverse=True
        )

        if not targets:
            return None

        return targets[0]

    def _sum_power(self, count_attacker: bool):
        return sum(unit.power * unit.pocet for unit in self.battle.units if unit.is_attacker_player == count_attacker)

    @staticmethod
    def _target_selection_rank_shooter_bonus(unit: Unit, prefer_shooting: bool):
        if prefer_shooting and unit.is_shooter():
            return 3.0

        return 1.0

    def _alive_units(self, attacker_units: bool, extra_condition=None) -> Generator[Unit, None, None]:
        if extra_condition is not None:
            return (unit for unit in self.battle.units if unit.is_attacker_player == attacker_units and unit.pocet > 0 and extra_condition(unit))

        return (unit for unit in self.battle.units if unit.is_attacker_player == attacker_units and unit.pocet > 0)

    def _log_spell_target_not_found(self, is_attacker: bool, spell: SpellInCombat) -> None:
        self.battle.log_event(CastSpell(is_attacker, target_found=False, spell_name=spell.name, success=True))

    def _log_spell_success(self, is_attacker: bool, spell: SpellInCombat, replace: Dict[str, object] = None, target: Unit = None):
        text = spell.text_template
        if target is not None:
            text = text.replace('xxJEDNOTKAxx', target.nazev)

        if replace is not None:
            for key, value in replace.items():
                text = text.replace(key, str(value))

        self.battle.log_event(CastSpell(is_attacker, True, True, spell.name, text))
