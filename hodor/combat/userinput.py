# -*- coding: utf-8 -*-
import math
import re
from typing import Iterable, List, Optional

from hodor.combat.combatmodel import UserInput, Unit, UnitStatic, UnitMetaType, UnitTargeting
from hodor.combat.logevent import AnsiColors
from hodor.combat.staticdata import gate_dynamic_power, unit_at_gates_new, gates_new_power, gate_keeper_aliases
from hodor.data.provider import static_unit_by_name, static_unit_by_id


def process_user_input(user_input: Iterable[UserInput],
                       allow_specials: bool = False,
                       initiative_coefficient_override: float = 1.1) -> List[Unit]:
    result = []

    for single_input in user_input:
        if single_input.unit_id:
            static = static_unit_by_id(single_input.unit_id)
        else:
            static = static_unit_by_name(single_input.name)
        if static.meta_type == UnitMetaType.SPECIAL and not allow_specials:
            print('%sUnit %s%s%s ignored, specials cannot participate in gates.%s' % (
                AnsiColors.BRIGHT_YELLOW,
                AnsiColors.GREEN, static.jmeno,
                AnsiColors.BRIGHT_YELLOW,
                AnsiColors.RESET))
            continue

        if static.meta_type not in (UnitMetaType.REGULAR, UnitMetaType.SPECIAL):
            print('%sUnit %s ignored, not a known regular unit%s' % (AnsiColors.RED, static.jmeno, AnsiColors.RESET))
            continue

        if single_input.count is not None:
            count = single_input.count
        elif single_input.power is not None:
            count = round(
                single_input.power / static.power / single_input.experience
            )
        else:
            raise Exception("Neither count nor power input.")

        result.append(
            single_unit(static, single_input.experience, count, True, initiative_coefficient_override)
        )

    return result


def single_unit(static: UnitStatic,
                experience: float,
                count: int,
                is_attacker_player: bool,
                initiative_coefficient: float,
                dynamic_power=1.0,
                forced_counters: int = None,
                name_override: str = None,
                targeting_override: str = None,
                ) -> Unit:
    armor = static.brneni * experience
    life = static.zivoty * experience
    initiative = static.iniciativa * initiative_coefficient

    armor_total = int(armor * count)
    life_total = life * count
    power = static.power * experience
    counters = forced_counters if forced_counters is not None else counters_for_experience(experience)

    name = static.jmeno if name_override is None else name_override
    targeting = UnitTargeting.DAMAGE
    if targeting_override:
        targeting = targeting_override

    unit = Unit(
        is_attacker_player, static.id, name,
        count, counters, static.dmg,
        armor_total, life, initiative, static.typ_ut,
        static.pohybl, power, static.power, static.druh, life_total, targeting,
        experience * 100
    )

    if dynamic_power != 1.0:
        unit.power *= dynamic_power
        unit.dmg *= dynamic_power
        unit.zivoty *= dynamic_power
        unit.celkem_zivotu *= dynamic_power
        unit.brneni_celkem *= dynamic_power
        unit.brneni_celkem_puvodni *= dynamic_power

    return unit


def calc_gate_dynamic_power(gate_number: int, players_passed: Optional[int] = None) -> float:
    dynamic_power_data = gate_dynamic_power.get(gate_number)

    if players_passed is None or dynamic_power_data is None:
        return 1.0
    elif players_passed >= dynamic_power_data['min_coef_count']:
        return dynamic_power_data['min_coef']
    else:
        return (
                dynamic_power_data['max_coef'] -
                (players_passed / dynamic_power_data['min_coef_count'] * (
                        dynamic_power_data['max_coef'] - dynamic_power_data['min_coef']))
        )


def new_gate_units(gate_number: int, players_passed: Optional[int] = None) -> List[List[Unit]]:
    result = []
    dynamic_power = calc_gate_dynamic_power(gate_number, players_passed)
    gate_power = gates_new_power[gate_number]

    for units_at_gate in unit_at_gates_new:
        gate_units = []
        for unit_at_gate in units_at_gate:
            unit = static_unit_by_id(unit_at_gate.unit_id)

            if unit_at_gate.count is not None:
                count = unit_at_gate.count
            elif unit_at_gate.percent_power is not None:
                count = math.ceil(gate_power * unit_at_gate.percent_power / unit.power)
            else:
                raise Exception("Got neither count nor percent_power for a gate unit definition")

            name_override = None
            # try to find name override
            if unit.rod is not None:
                name_override = '%s %s' % (gate_keeper_aliases[gate_number][unit.rod], unit.jmeno)

            gate_units.append(
                single_unit(unit, 1.0, count, False, 0.9, dynamic_power, name_override=name_override)
            )

        result.append(gate_units)
    return result


def counters_for_experience(experience: float) -> int:
    if experience < 0.7:
        return 1

    if experience < 0.95:
        return 2

    return 3


def read_file(file_name: str) -> str:
    with open(file_name, 'r', encoding="utf-8") as file:
        return file.read()


def parse_input_file_autodetect(data: str) -> List[UserInput]:
    first_line = data.strip().split("\n")[0].strip()
    if re.match(r'^[^;]+;\d+(\.\d+)?;(p=)?\d+$', first_line):
        return parse_csv_gate(data)

    return parse_economics(data)


def parse_csv_gate(data: str) -> List[UserInput]:
    result = []

    lines = data.strip().split('\n')

    for line in lines:
        if not line:
            continue
        parts = line.split(";")
        parts = list(part.strip() for part in parts)

        # power instead of count
        m = re.match(r'p=(\d+)$', parts[2])
        if m:
            result.append(UserInput(name=parts[0], experience=float(parts[1]), power=int(m[1])))
        else:
            result.append(UserInput(name=parts[0], experience=float(parts[1]), count=int(parts[2])))

    return result


def parse_economics(data: str) -> List[UserInput]:
    result = []

    match = re.search(r'^Jednotka\tBarva\t.*?^(.*?)^CELKEM ZA TAH\t', data, re.MULTILINE | re.DOTALL | re.IGNORECASE)
    if not match:
        raise Exception('Cannot find key words in input file ("Jednotka\tBarva" or "CELKEM ZA TAH"), cannot continue.')

    lines = match.group(1).strip().split('\n')

    for line in lines:
        parts = line.split("\t")
        result.append(UserInput(name=parts[0], experience=float(parts[2]) / 100.0, count=int(parts[-4])))

    return result


def parse_json(units: List[dict]) -> List[UserInput]:
    result = []

    for unit in units:
        result.append(UserInput(unit_id=unit['id'], experience=unit['xp'], count=unit['count']))

    return result
