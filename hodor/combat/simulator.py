from copy import deepcopy
from typing import List, Optional

from hodor.combat.battle import Battle
from hodor.combat.combatmodel import Unit
from hodor.combat.userinput import new_gate_units


def as_roman_num(index: int) -> str:
    roman = ['I', 'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'IX', 'X',
             'XI', 'XII', 'XIII', 'XIV', 'XV', 'XVI', 'XVII', 'XVIII', 'XIX', 'XX',
             'XXI', 'XXII', 'XXIII', 'XXIV', 'XXV', 'XXVI', 'XXVII', 'XXVIII', 'XXIX', 'XXX',
             'XXXI', 'XXXII', 'XXXIII', 'XXXIV', 'XXXV', 'XXXVI', 'XXXVII', 'XXXVIII', 'XXXIX', 'XL',
             'XLI', 'XLII']

    if index < 0 or index >= len(roman):
        return '%d' % index

    return roman[index]


def simulate_all(attacker_units: List[Unit], gate_number: int, players_passed: Optional[int] = None) -> List[Battle]:
    gate_units_sets = new_gate_units(gate_number=gate_number, players_passed=players_passed)
    battles = []

    for index, gate_units in enumerate(gate_units_sets):
        attacker_units_copy = deepcopy(attacker_units)
        battle = Battle(attacker_units_copy + gate_units, gate_number=gate_number, quiet=True, label=as_roman_num(index))
        battle.execute_battle()
        battles.append(battle)

    return battles
