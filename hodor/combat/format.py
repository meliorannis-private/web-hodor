# -*- coding: utf-8 -*-


def nft(number: int, thousand_separator=' ') -> str:
    return '{:,}'.format(number).replace(',', thousand_separator)
