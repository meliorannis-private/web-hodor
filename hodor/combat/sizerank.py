import math

MIN_POWER = 0.42
MAX_POWER = 320
MIN_LOG = math.log10(MIN_POWER)
MAX_LOG = math.log10(MAX_POWER)


def size_bonus(attacker_power: float, defender_power: float) -> float:
    """
    Bonus/penalty for attacker vs defender

    >>> size_bonus(2, 3) # S vs S
    1.0

    >>> size_bonus(20, 30) # M vs M
    1.0

    >>> size_bonus(200, 300) # L vs L
    1.0

    >>> size_bonus(20, 2) # M vs S
    1.25

    >>> size_bonus(200, 20) # L vs M
    1.25

    >>> size_bonus(2, 200) # S vs L
    1.25

    >>> size_bonus(2, 20) # S vs M
    0.75

    >>> size_bonus(20, 200) # M vs L
    0.75

    >>> size_bonus(200, 2) # L vs S
    0.75
    """

    attacker_rank = rank(attacker_power)
    defender_rank = rank(defender_power)

    diff = (attacker_rank - defender_rank) % 3

    if diff == 0:
        return 1.0

    if diff == 1:
        return 1.25

    return 0.75


def rank(power: float) -> int:
    """
    Rank of power, small = 0, medium = 1, large = 2.

    >>> rank(0.42)
    0

    >>> rank(0.0)
    0

    >>> rank(9.99)
    0

    >>> rank(10.0)
    0

    >>> rank(10.01)
    1

    >>> rank(100.0)
    1

    >>> rank(100.1)
    2

    >>> rank(320)
    2
    """

    if power <= 10:
        return 0

    if power <= 100:
        return 1

    return 2
