# -*- coding: utf-8 -*-

from typing import List

from hodor.combat.combatmodel import NewUnitAtGate

unit_at_gates_new = [
    [
        NewUnitAtGate(5018, percent_power=0.3),  # Arkana
        NewUnitAtGate(6035, percent_power=0.3),  # Mušketýr
        NewUnitAtGate(5007, percent_power=0.3),  # Ledový obr
        NewUnitAtGate(6018, percent_power=0.02),  # Rudý drak
        NewUnitAtGate(6026, percent_power=0.02),  # Lich King
        NewUnitAtGate(6027, percent_power=0.02),  # Trebuchet
        NewUnitAtGate(2009, percent_power=0.02),  # Archanděl
        NewUnitAtGate(5518, percent_power=0.02),  # Sfinga
    ],
    [
        NewUnitAtGate(3006, percent_power=0.2),  # Ohnivec
        NewUnitAtGate(6003, percent_power=0.2),  # Chalífa
        NewUnitAtGate(4002, percent_power=0.2),  # Mámení
        NewUnitAtGate(2005, percent_power=0.2),  # Apoštol
        NewUnitAtGate(1009, percent_power=0.05),  # Lich
        NewUnitAtGate(6001, percent_power=0.15),  # Bludička
        NewUnitAtGate(5009, count=1),  # Polární bouře
        NewUnitAtGate(1024, count=1),  # Dračí démon
        NewUnitAtGate(4013, count=1),  # Homunkulus
        NewUnitAtGate(2005, count=1),  # Apoštol
        NewUnitAtGate(2006, count=1),  # Anděl
        NewUnitAtGate(3016, count=1),  # Zelený drak
    ],
    [
        NewUnitAtGate(6019, percent_power=0.2),  # Nemrtvý drak
        NewUnitAtGate(6023, percent_power=0.2),  # Vodní drak
        NewUnitAtGate(5018, percent_power=0.2),  # Arkana
        NewUnitAtGate(6017, percent_power=0.2),  # Mantikora
        NewUnitAtGate(6001, percent_power=0.2),  # Bludička
        NewUnitAtGate(1024, count=1),  # Dračí démon
        NewUnitAtGate(1006, count=1),  # Spektra
        NewUnitAtGate(1019, count=1),  # Ďábel
    ],
    [
        NewUnitAtGate(4018, percent_power=0.4),  # Kyklop
        NewUnitAtGate(6009, percent_power=0.3),  # Lev
        NewUnitAtGate(2009, percent_power=0.03),  # Archanděl
        NewUnitAtGate(6018, percent_power=0.03),  # Rudý drak
        NewUnitAtGate(1023, percent_power=0.03),  # Tempest
        NewUnitAtGate(6027, percent_power=0.03),  # Trebuchet
        NewUnitAtGate(5018, percent_power=0.03),  # Arkana
        NewUnitAtGate(6026, percent_power=0.03),  # Lich King
        NewUnitAtGate(1014, percent_power=0.03),  # Přízrak
        NewUnitAtGate(3012, percent_power=0.03),  # Elfí lučištník
        NewUnitAtGate(3003, percent_power=0.03),  # Dryáda
        NewUnitAtGate(1007, percent_power=0.03),  # Banshee
        NewUnitAtGate(3017, count=1),  # Gargantua
        NewUnitAtGate(3008, count=1),  # Jednorožec
        NewUnitAtGate(2014, count=1),  # Husar
        NewUnitAtGate(4009, count=1),  # Titán
        NewUnitAtGate(2018, count=1),  # Obrněná jízda
        NewUnitAtGate(2008, count=1),  # Paladin
    ],
    [
        NewUnitAtGate(4009, percent_power=0.2),  # Titán
        NewUnitAtGate(5008, percent_power=0.2),  # Remorhaz
        NewUnitAtGate(6008, percent_power=0.2),  # Zlovlk
        NewUnitAtGate(6009, percent_power=0.2),  # Lev
        NewUnitAtGate(5009, percent_power=0.2),  # Polární bouře
        NewUnitAtGate(1024, count=1),  # Dračí démon
        NewUnitAtGate(6008, count=1),  # Zlovlk
        NewUnitAtGate(4018, count=1),  # Kyklop
        NewUnitAtGate(6034, count=1),  # Gryfí jezdec
        NewUnitAtGate(5017, count=1),  # Efreet
        NewUnitAtGate(4007, count=1),  # Zlatá panna
        NewUnitAtGate(3008, count=1),  # Jednorožec
        NewUnitAtGate(3017, count=1),  # Gargantua
    ],
    [
        NewUnitAtGate(1003, percent_power=0.25),  # Stín
        NewUnitAtGate(1020, percent_power=0.25),  # Noční můra
        NewUnitAtGate(6016, percent_power=0.1),  # Gladiátor
        NewUnitAtGate(5516, percent_power=0.1),  # Chariot
        NewUnitAtGate(2015, percent_power=0.1),  # Bezhlavý rytíř
        NewUnitAtGate(6027, percent_power=0.05),  # Trebuchet
        NewUnitAtGate(6018, percent_power=0.05),  # Rudý drak
        NewUnitAtGate(6038, percent_power=0.1),  # Revenant
        NewUnitAtGate(1024, count=1),  # Dračí démon
        NewUnitAtGate(6008, count=1),  # Zlovlk
        NewUnitAtGate(4018, count=1),  # Kyklop
        NewUnitAtGate(6034, count=1),  # Gryfí jezdec
        NewUnitAtGate(5017, count=1),  # Efreet
        NewUnitAtGate(4007, count=1),  # Zlatá panna
        NewUnitAtGate(3008, count=1),  # Jednorožec
        NewUnitAtGate(3017, count=1),  # Gargantua
    ],
    [
        NewUnitAtGate(3001, percent_power=0.2),  # Satyr
        NewUnitAtGate(3010, percent_power=0.2),  # Vlk
        NewUnitAtGate(3013, percent_power=0.2),  # Medvěd
        NewUnitAtGate(3018, percent_power=0.1),  # Zlatý drak
        NewUnitAtGate(3015, percent_power=0.05),  # Obří dikobraz
        NewUnitAtGate(3012, percent_power=0.05),  # Elfí lučištník
        NewUnitAtGate(3011, percent_power=0.05),  # Duch stromu
        NewUnitAtGate(3007, percent_power=0.05),  # Ent
        NewUnitAtGate(3002, percent_power=0.05),  # Víla
        NewUnitAtGate(3003, percent_power=0.05),  # Dryáda
        NewUnitAtGate(1024, count=1),  # Dračí démon
        NewUnitAtGate(6008, count=1),  # Zlovlk
        NewUnitAtGate(4018, count=1),  # Kyklop
        NewUnitAtGate(6034, count=1),  # Gryfí jezdec
        NewUnitAtGate(5017, count=1),  # Efreet
        NewUnitAtGate(4007, count=1),  # Zlatá panna
        NewUnitAtGate(3008, count=1),  # Jednorožec
        NewUnitAtGate(3017, count=1),  # Gargantua
    ],
    [
        NewUnitAtGate(6008, percent_power=0.15),  # Zlovlk
        NewUnitAtGate(6009, percent_power=0.15),  # Lev
        NewUnitAtGate(5018, percent_power=0.1),  # Arkana
        NewUnitAtGate(6035, percent_power=0.1),  # Mušketýr
        NewUnitAtGate(5007, percent_power=0.1),  # Ledový obr
        NewUnitAtGate(6022, percent_power=0.1),  # Ghoul
        NewUnitAtGate(2007, percent_power=0.1),  # Zealot
        NewUnitAtGate(6024, percent_power=0.1),  # Válečný slon
        NewUnitAtGate(6015, percent_power=0.1),  # Vozová hradba
        NewUnitAtGate(3017, count=1),  # Gargantua
        NewUnitAtGate(3008, count=1),  # Jednorožec
        NewUnitAtGate(1024, count=1),  # Dračí démon
        NewUnitAtGate(4018, count=1),  # Kyklop
        NewUnitAtGate(6034, count=1),  # Gryfí jezdec
        NewUnitAtGate(5009, count=1),  # Polární bouře
        NewUnitAtGate(6011, count=1),  # Nomád
        NewUnitAtGate(6036, count=1),  # Těžká kavalérie
        NewUnitAtGate(6033, count=1),  # Lehká kavalérie
    ],
    [
        NewUnitAtGate(5008, percent_power=0.4),  # Remorhaz
        NewUnitAtGate(5007, percent_power=0.2),  # Ledový obr
        NewUnitAtGate(5018, percent_power=0.2),  # Arkana
        NewUnitAtGate(1023, percent_power=0.1),  # Tempest
        NewUnitAtGate(1007, percent_power=0.05),  # Banshee
        NewUnitAtGate(6018, percent_power=0.025),  # Rudý drak
        NewUnitAtGate(6026, percent_power=0.025),  # Lich King
        NewUnitAtGate(4018, count=1),  # Kyklop
        NewUnitAtGate(5009, count=1),  # Polární bouře
    ],
]  # type: List[List[NewUnitAtGate]]

gates_new_power = {
    1: 75_000,
    2: 170_000,
    3: 260_000,
    4: 350_000,
    5: 450_000,
    6: 600_000,
    7: 800_000,
    8: 1_050_000,
}

gate_dynamic_power = {
    1: {
        'max_coef': 1.0,
        'min_coef': 0.7,
        'min_coef_count': 100,
    },
    2: {
        'max_coef': 1.0,
        'min_coef': 0.7,
        'min_coef_count': 80,
    },
    3: {
        'max_coef': 1.0,
        'min_coef': 0.7,
        'min_coef_count': 70,
    },
    4: {
        'max_coef': 1.0,
        'min_coef': 0.7,
        'min_coef_count': 50,
    },
    5: {
        'max_coef': 1.0,
        'min_coef': 0.75,
        'min_coef_count': 30,
    },
    6: {
        'max_coef': 1.0,
        'min_coef': 0.85,
        'min_coef_count': 20,
    },
    7: {
        'max_coef': 1.0,
        'min_coef': 0.9,
        'min_coef_count': 10,
    },
    8: {
        'max_coef': 1.0,
        'min_coef': 0.9,
        'min_coef_count': 5,
    },
}

gate_keeper_aliases = {
    1: ['Klothův', 'Klothova', 'Klothovo'],
    2: ['Lachesisův', 'Lachesova', 'Lachesovo'],
    3: ['Atropův', 'Atropova', 'Atropovo'],
    4: ['Caermův', 'Caermova', 'Caermino'],
    5: ['Argův', 'Argova', 'Argovo'],
    6: ['Furiin', 'Furiina', 'Furiino'],
    7: ['Celestin', 'Celestina', 'Celestino'],
    8: ['Tařin', 'Tařina', 'Tařino'],
}
