from django.apps import AppConfig


class HodorConfig(AppConfig):
    name = 'hodor'
