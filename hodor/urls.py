from django.conf import settings
from django.urls import path
from django.views.generic import RedirectView

from . import views

app_name = 'hodor'

urlpatterns = []

if settings.WEB_HODOR_ENABLE_WEB:
    urlpatterns = [
        path('', RedirectView.as_view(pattern_name='hodor:tournament_home')),
        path('hodor/', views.simul, name='simul'),
        path('tournaments/', views.tournament_home, name='tournament_home'),
        path('tournaments/builder/', views.tournament_builder, name='tournament_builder'),
        path('tournaments/simul/', views.tournament_simul, name='tournament_simul'),
        path('tournaments/<int:tournament_id>-<slug:tournament_slug>/', views.tournament_detail, name='tournament_detail'),
        path('tournaments/<int:tournament_id>-<slug:tournament_slug>/result/', views.tournament_result, name='tournament_result'),
        path('tournaments/<int:tournament_id>-<slug:tournament_slug>/admin/', views.tournament_admin, name='tournament_admin'),
        path('tournaments/<int:tournament_id>-<slug:tournament_slug>/execute/', views.tournament_execute, name='tournament_execute'),
        path('tournaments/<int:tournament_id>-<slug:tournament_slug>/validate/', views.tournament_validate, name='tournament_validate'),
        path('tournaments/<int:tournament_id>-<slug:tournament_slug>/result/<int:player_id>/', views.tournament_result_player, name='tournament_result_player'),
        path('tournaments/<int:tournament_id>-<slug:tournament_slug>/result/<int:attacker_id>-vs-<int:defender_id>/', views.tournament_result_match, name='tournament_result_match'),
        path('ajax/units/', views.ajax_units, name='ajax_units'),
        path('info/units/', views.info_units, name='info_units'),
        path('info/sizes/', views.info_sizes, name='info_sizes'),
    ]

urlpatterns.append(
    path('ajax/simulate-gate/', views.ajax_simulate_gate, name='ajax_simulate_gate')
)
