$(function () {
    function remove_from_array(array, value) {
        let index = array.indexOf(value);
        if (index !== -1) {
            array.splice(index, 1);
        }
    }

    // see https://stackoverflow.com/a/30810322/600169
    function copyTextToClipboard(text) {
        let textArea = document.createElement("textarea");

        //
        // *** This styling is an extra step which is likely not required. ***
        //
        // Why is it here? To ensure:
        // 1. the element is able to have focus and selection.
        // 2. if the element was to flash render it has minimal visual impact.
        // 3. less flakyness with selection and copying which **might** occur if
        //    the textarea element is not visible.
        //
        // The likelihood is the element won't even render, not even a
        // flash, so some of these are just precautions. However in
        // Internet Explorer the element is visible whilst the popup
        // box asking the user for permission for the web page to
        // copy to the clipboard.
        //

        // Place in the top-left corner of screen regardless of scroll position.
        textArea.style.position = 'fixed';
        textArea.style.top = 0;
        textArea.style.left = 0;

        // Ensure it has a small width and height. Setting to 1px / 1em
        // doesn't work as this gives a negative w/h on some browsers.
        textArea.style.width = '2em';
        textArea.style.height = '2em';

        // We don't need padding, reducing the size if it does flash render.
        textArea.style.padding = 0;

        // Clean up any borders.
        textArea.style.border = 'none';
        textArea.style.outline = 'none';
        textArea.style.boxShadow = 'none';

        // Avoid flash of the white box if rendered for any reason.
        textArea.style.background = 'transparent';


        textArea.value = text;

        document.body.appendChild(textArea);
        textArea.focus();
        textArea.select();

        try {
            let successful = document.execCommand('copy');
            let msg = successful ? 'successful' : 'unsuccessful';
            console.log('Copying text command was ' + msg);
        } catch (err) {
            console.log('Oops, unable to copy');
        }

        document.body.removeChild(textArea);
    }

    const MAX_HISTORY = 99;

    let history_json = window.localStorage.getItem('history');
    let history;
    if (history_json === null) {
        history = {
            'attacker': [],
            'defender': [],
        };

        // possibly migrate old history
        let history_attacker_json = window.localStorage.getItem('history_attacker');
        if (history_attacker_json !== null) {
            JSON.parse(history_attacker_json).forEach(function (entry) {
                history['attacker'].push({
                    'army': entry,
                    'support_xp': '0',
                    'spells': []
                });
            });
        }
        let history_defender_json = window.localStorage.getItem('history_defender');
        if (history_defender_json !== null) {
            JSON.parse(history_defender_json).forEach(function (entry) {
                history['defender'].push({
                    'army': entry,
                    'support_xp': '0',
                    'spells': [],
                });
            });
        }

        if (history['attacker'].length === 0) {
            history['attacker'].push({
                'army': "Kyklop;0.4;333\nBanshee;1;139\nHoufnice;1;61\nObří dikobraz;1;83;pocet\nMystra;1;46;pocet\nKamenný obr;1;300;pocet\nNymfa;1;2001\nMoira;1;284\nBalista;1;64\nSfinga;1;41",
                'support_xp': '100',
                'spells': [],
            });
        }
        if (history['defender'].length === 0) {
            history['defender'].push({
                'army': "Stín;0.5;25000;dmg\nBezhlavý rytíř;0.4;1;dmg\nGryf;0.4;1;dmg\nWyverna;0.4;1;dmg\nSfinga;1;20;dmg\nVíla;0.98;500;dmg\nBílý drak;1;67;dmg\nMystra;1;55;dmg\nUpír;0.4;1;dmg\nMeluzína;0.4;1;dmg",
                'support_xp': '100',
                'spells': [],
            });
        }
    } else {
        history = JSON.parse(history_json);
    }
    console.log(history);

    let attacker_selected_army_index = 0;
    let defender_selected_army_index = 0;

    $('#attacker_history_length').text(history['attacker'].length);
    $('#defender_history_length').text(history['defender'].length);

    console.log('usb2', window.localStorage.getItem('use_size_bonus'));
    $('#use_size_bonus')
        .prop('checked', window.localStorage.getItem('use_size_bonus') === 'true')
        .change(function () {
            window.localStorage.setItem('use_size_bonus', $(this).prop('checked'));
            console.log('usb', window.localStorage.getItem('use_size_bonus'));
        });


    function propagate_history(selector, index) {
        let entry = history[selector][index];

        $('#' + selector + '_army').val(entry['army']);
        $('#' + selector + '_army_support_xp').val(entry['support_xp']);
        $('#' + selector + '_history_index').text(index + 1);

        $('#history_prev_' + selector + '').attr('disabled', index === 0);
        $('#history_next_' + selector + '').attr('disabled', index === history[selector].length - 1);

        for (let i = 0; i < 4; ++i) {
            if (i < entry['spells'].length) {
                $('#spell_select_' + selector + '_' + i).val(entry['spells'][i]['spell_id']);
                $('#spell_target_' + selector + '_' + i).val(entry['spells'][i]['target']);
                $('#spell_xp_' + selector + '_' + i).val(entry['spells'][i]['xp']);
            } else {
                $('#spell_select_' + selector + '_' + i).val(0);
                $('#spell_target_' + selector + '_' + i).val('dmg');
                $('#spell_xp_' + selector + '_' + i).val('100');
            }
        }
    }

    $('#history_prev_attacker').click(function () {
        attacker_selected_army_index = Math.max(0, attacker_selected_army_index - 1);
        propagate_history('attacker', attacker_selected_army_index);
    }).click();

    $('#history_next_attacker').click(function () {
        attacker_selected_army_index = Math.min(history['attacker'].length - 1, attacker_selected_army_index + 1);
        propagate_history('attacker', attacker_selected_army_index);
    });

    $('#history_prev_defender').click(function () {
        defender_selected_army_index = Math.max(0, defender_selected_army_index - 1);
        propagate_history('defender', defender_selected_army_index);
    }).click();

    $('#history_next_defender').click(function () {
        defender_selected_army_index = Math.min(history['defender'].length - 1, defender_selected_army_index + 1);
        propagate_history('defender', defender_selected_army_index);
    });

    $('#attacker-copy-to-clipboard').click(function () {
        let text = $('#attacker_army').val().trim();
        text += "\n----------------";

        text += "\n" + $('#spell_select_attacker_0').val() + ";" + (parseInt($('#spell_xp_attacker_0').val()) / 100.0) + ";" + $('#spell_target_attacker_0').val();
        text += "\n" + $('#spell_select_attacker_1').val() + ";" + (parseInt($('#spell_xp_attacker_1').val()) / 100.0) + ";" + $('#spell_target_attacker_1').val();
        text += "\n" + $('#spell_select_attacker_2').val() + ";" + (parseInt($('#spell_xp_attacker_2').val()) / 100.0) + ";" + $('#spell_target_attacker_2').val();
        text += "\n" + $('#spell_select_attacker_3').val() + ";" + (parseInt($('#spell_xp_attacker_3').val()) / 100.0) + ";" + $('#spell_target_attacker_3').val();

        copyTextToClipboard(text);

        $('#copied-to-clipboard-alert').show().delay(2000).fadeOut({duration: 500});
    });

    $('#defender-copy-to-clipboard').click(function () {
        let text = $('#defender_army').val().trim();
        text += "\n----------------";

        text += "\n" + $('#spell_select_defender_0').val() + ";" + (parseInt($('#spell_xp_defender_0').val()) / 100.0) + ";" + $('#spell_target_defender_0').val();
        text += "\n" + $('#spell_select_defender_1').val() + ";" + (parseInt($('#spell_xp_defender_1').val()) / 100.0) + ";" + $('#spell_target_defender_1').val();
        text += "\n" + $('#spell_select_defender_2').val() + ";" + (parseInt($('#spell_xp_defender_2').val()) / 100.0) + ";" + $('#spell_target_defender_2').val();
        text += "\n" + $('#spell_select_defender_3').val() + ";" + (parseInt($('#spell_xp_defender_3').val()) / 100.0) + ";" + $('#spell_target_defender_3').val();

        copyTextToClipboard(text);

        $('#copied-to-clipboard-alert').show().delay(2000).fadeOut({duration: 500});
    });

    $('#submit').click(function () {
        let army_attacker = $('#attacker_army').val();
        let support_xp_attacker = $('#attacker_army_support_xp').val();
        let entry_attacker = {
            'army': army_attacker,
            'support_xp': support_xp_attacker,
            'spells': [
                {'spell_id': $('#spell_select_attacker_0').val(), 'target': $('#spell_target_attacker_0').val(), 'xp': $('#spell_xp_attacker_0').val()},
                {'spell_id': $('#spell_select_attacker_1').val(), 'target': $('#spell_target_attacker_1').val(), 'xp': $('#spell_xp_attacker_1').val()},
                {'spell_id': $('#spell_select_attacker_2').val(), 'target': $('#spell_target_attacker_2').val(), 'xp': $('#spell_xp_attacker_2').val()},
                {'spell_id': $('#spell_select_attacker_3').val(), 'target': $('#spell_target_attacker_3').val(), 'xp': $('#spell_xp_attacker_3').val()},
            ]
        }
        remove_from_array(history['attacker'], entry_attacker);
        history['attacker'].unshift(entry_attacker);
        history['attacker'].splice(MAX_HISTORY, history['attacker'].length - MAX_HISTORY); // trim history

        let army_defender = $('#defender_army').val();
        let support_xp_defender = $('#defender_army_support_xp').val();
        let entry_defender = {
            'army': army_defender,
            'support_xp': support_xp_defender,
            'spells': [
                {'spell_id': $('#spell_select_defender_0').val(), 'target': $('#spell_target_defender_0').val(), 'xp': $('#spell_xp_defender_0').val()},
                {'spell_id': $('#spell_select_defender_1').val(), 'target': $('#spell_target_defender_1').val(), 'xp': $('#spell_xp_defender_1').val()},
                {'spell_id': $('#spell_select_defender_2').val(), 'target': $('#spell_target_defender_2').val(), 'xp': $('#spell_xp_defender_2').val()},
                {'spell_id': $('#spell_select_defender_3').val(), 'target': $('#spell_target_defender_3').val(), 'xp': $('#spell_xp_defender_3').val()},
            ]
        }
        remove_from_array(history['defender'], entry_defender);
        history['defender'].unshift(entry_defender);
        history['defender'].splice(MAX_HISTORY, history['defender'].length - MAX_HISTORY); // trim history

        window.localStorage.setItem('history', JSON.stringify(history));
    });
});