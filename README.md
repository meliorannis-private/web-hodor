# Hodor

Hodor -- the Gate Keeper. Web version

## Requirements

### Local dev

Python 3.6+, requirements as per `requirements.txt`

### Docker

Just build && run

```
docker build . -t hodor
docker run -it --rm -p8000:8000 hodor
```

it will run at localhost:8000.

## Units from DB

Export as JSON from

```sql
SELECT jmeno,id,dmg,brneni,zivoty,iniciativa,typ_ut,pohybl,power,druh,iq FROM jednotky WHERE ...
```

and then

```python
import re

data = [
  {
    "jmeno": "<span class=spec_unit>Fúrie Megaira</span>",
    "id": 10014,
    "dmg": 7200,
    "brneni": 8000,
    "zivoty": 16000,
    "iniciativa": 49,
    "typ_ut": "B",
    "pohybl": 1,
    "power": 400,
    "druh": "L",
    "iq": 8
  },
]

for j in data:
    jmeno = re.sub(r'(<span[^>]+>)?([^<>]+)(</span>)?', r'\2', j['jmeno'])
    print("UnitStatic(%d, '%s', %.2f, %.2f, %.2f, %d, '%s', %d, %.2f, '%s', %d, UnitMetaType.SPECIAL),"
          % (j['id'], jmeno, j['dmg'], j['brneni'], j['zivoty'], j['iniciativa'], j['typ_ut'], j['pohybl'], j['power'], j['druh'], j['iq']))
```

## Units for Builder (javascript)

Just dump this query as JSON:

```sql
SELECT jednotky.id,
       jednotky.jmeno,
       jednotky.dmg,
       jednotky.brneni,
       jednotky.zivoty,
       jednotky.iniciativa,
       jednotky.typ_ut,
       jednotky.pohybl,
       jednotky.power,
       jednotky.druh,
       jednotky.iq,
       jednotky.barva,
       jednotky.specializace,
       budovy.max * jednotky.jedn_za_tu AS jedn_za_tu,
       jednotky.plat_z,
       jednotky.plat_m,
       jednotky.plat_l
FROM jednotky
     LEFT JOIN budovy ON budovy.id = jednotky.rekrut_bud
WHERE jednotky.id BETWEEN 1000 AND 7000
ORDER BY jednotky.id
```

into a file :)