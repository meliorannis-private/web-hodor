create table jednotky
(
    id                smallint unsigned default 0    not null primary key,
    jmeno             varchar(128)      default ''   not null,
    barva             varchar           default ''   not null,
    specializace      varchar           default ''   not null,
    dmg               float(10, 2)      default 0.00 not null,
    brneni            float(10, 2)      default 0.00 not null,
    zivoty            float(10, 2)      default 0.00 not null,
    plat_z            float(10, 2)      default 0.00 not null,
    plat_m            float(10, 2)      default 0.00 not null,
    plat_l            float(10, 2)      default 0.00 not null,
    rekrut_z          float(10, 2)      default 0.00 not null,
    rekrut_m          float(10, 2)      default 0.00 not null,
    rekrut_l          float(10, 2)      default 0.00 not null,
    iniciativa        tinyint           default 0    not null,
    typ_ut            varchar           default ''   not null,
    pohybl            tinyint           default 0    not null,
    power             float(10, 2)      default 0.00 not null,
    druh              varchar           default ''   not null,
    rekrut_bud        smallint unsigned default 0    not null,
    jedn_za_tu        float(10, 2)      default 0.00 not null,
    abs_nenap_0       smallint unsigned default 0    not null,
    abs_nenap_1       smallint unsigned default 0    not null,
    abs_nenap_2       smallint unsigned default 0    not null,
    abs_nenap_3       smallint unsigned default 0    not null,
    abs_nenap_4       smallint unsigned default 0    not null,
    abs_nenap_5       smallint unsigned default 0    not null,
    abs_nenap_6       smallint unsigned default 0    not null,
    abs_nenap_7       smallint unsigned default 0    not null,
    abs_nenap_8       smallint unsigned default 0    not null,
    nosnost_z         smallint unsigned default 0    not null,
    nosnost_m         smallint unsigned default 0    not null,
    nosnost_l         smallint unsigned default 0    not null,
    iq                tinyint unsigned  default 0    not null,
    zobraz_utok       tinyint           default 0    not null,
    zobraz_zivoty     tinyint           default 0    not null,
    zobraz_brneni     tinyint           default 0    not null,
    zobraz_iniciativa tinyint           default 0    not null,
    rod               tinyint                        null
);

create table budovy
(
    id             smallint           default 0      not null primary key,
    nazev          char(64)           default ''     not null,
    power          varchar(32)        default '0.00' not null,
    penize_za_tu   float(16, 4)       default 0.0000 not null,
    mana_za_tu     float(10, 2)       default 0.00   not null,
    lide_za_tu     float(10, 2)       default 0.00   not null,
    danit          tinyint unsigned   default 0      not null,
    staveni_tu     int unsigned       default 0      not null,
    obsahuje_lidi  mediumint unsigned default 0      not null,
    vezni_lidi     smallint unsigned  default 0      not null,
    plocha         tinyint unsigned   default 0      not null,
    mana_max       smallint unsigned  default 0      not null,
    cena_z         int unsigned       default 0      not null,
    cena_m         int unsigned       default 0      not null,
    cena_l         int unsigned       default 0      not null,
    vyzaduje       char(16)           default ''     not null,
    barva          char(2)            default ''     not null,
    specializace   char(2)            default ''     not null,
    max            char(255)          default ''     not null,
    typ_budovy     tinyint unsigned   default 0      not null,
    rekrut_bud_typ tinyint unsigned   default 0      not null,
    popis          char(255)          default ''     not null
);

create table kouzla
(
    id               smallint unsigned  default 0  not null primary key,
    nazev            char(150)          default '' not null,
    barva            char               default '' not null,
    special          char               default '' not null,
    druh             char(3)            default '' not null,
    mana_sesl        int unsigned       default 0  not null,
    sesl_tu          smallint unsigned  default 0  not null,
    sesl_tu_posl     tinyint unsigned   default 4  not null,
    trvani_tu        smallint unsigned  default 0  not null,
    ucinek           text                          not null,
    popis            char(255)          default '' not null,
    text_pri_seslani char(255)          default '' not null,
    cena_zl          mediumint unsigned default 0  not null,
    cena_mn          mediumint unsigned default 0  not null,
    vynalezani_tu    mediumint unsigned default 0  not null,
    brana            tinyint unsigned   default 0  not null,
    SKM              tinyint unsigned   default 0  not null,
    SKB              tinyint unsigned   default 0  not null
);

